var myApp = angular.module('myApp', ['ngRoute'])

myApp.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/about', {
            templateUrl: 'template/about.html',
            controller: 'aboutController'
        })
    .otherwise({
        redirectTo: 'about'
    })
}])


// About Controller

myApp.controller('aboutController', ($scope, $http) =>{
    $scope.pageHeading = "About"
})